package com.tcs.app;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class EmployeeTest {

    @Test
    public void getFirstName() {

        String firstName = "Miguel";
        String expected = "Miguel";

        Employee employee = new Employee();
        employee.setFirstName(firstName);

        String actual = employee.getFirstName();

        Assert.assertEquals(expected, actual);
    }

}